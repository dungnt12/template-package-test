import postcss from 'rollup-plugin-postcss';
import typescript from 'rollup-plugin-typescript2'

import pkg from './package.json'

export default {
  input: 'src/index.ts',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
      strict: false
    }
  ],
  plugins: [
    postcss({
      extract: false,
      modules: {
        generateScopedName: "[hash:base64:6]",
      },
      use: ['sass'],
    }),
    typescript()
  ],
  external: [
    '@shopify/polaris/locales/en.json', '@shopify/polaris', '@shopify/polaris-icons',
    'next', 'next/link', 'next/router',
    'react', 'react-dom',
    'mobx', 'mobx-react'
  ]
}
