const { execSync } = require('child_process');
const { resolve } = require('path');
const fs = require("fs"); // Or `import fs from "fs";` with ESM

const root = resolve(__dirname, '..');
const run = (cmd) => execSync(cmd, { stdio: 'inherit', cwd: root });

console.log('Move file to packages')
run(`set NODE_ENV=production && rm -rf dist && mkdir dist && npx babel src/components/ --out-dir packages/src --copy-files --ignore *.mdx`);

if (!fs.existsSync(`${root}/packages/node_modules`)) {
  console.log('Not found node_modules, we will install it, wait a minutes :D');
  run(`cd ./packages && npm install`);
}

console.log('Build package');
run(`cd ./packages && rollup -c`);
run(`cd ./packages`);
// // Custom for login
// // run(`cd ./packages && npm login --registry https://pkg.awe7.com:443`);
// run(`cd ./packages && npm version patch`);
// run(`cd ./packages && npm publish --registry https://pkg.awe7.com:443`);
// run(`cd ./packages && git add .`);
// run(`cd ./packages && git commit -m "New version"`);
// run(`cd ./packages && git push`);
