import * as React from 'react';
import { render } from 'react-dom';
import { observer } from 'mobx-react';
// import { connectReduxDevtools } from 'mst-middlewares';

import AppProvider from './components/wrapper';

// connectReduxDevtools(require('remotedev'), fontIconModel);

const Playground: React.FC = observer(() => {
  return (
    <AppProvider>
      <h1>HELLO</h1>
    </AppProvider>
  );
});

const rootElement = document.getElementById('root');
render(<Playground />, rootElement);
