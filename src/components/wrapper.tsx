import * as React from 'react';
import { AppProvider as AppProviderShopify } from '@shopify/polaris';
import enTranslations from '@shopify/polaris/locales/en.json';

const AppProvider: React.FC = ({ children }) => {
  return (
    <AppProviderShopify i18n={enTranslations}>
      {children}
    </AppProviderShopify>
  );
};

export default AppProvider;
